#include <stdio.h>
#include <stdlib.h>

int
main ()
{
    int nr = 5;         //mélység
    double **tm;        //létrehozom azt a pointer amivel hivatkozni fogok majd a mátrixomra

    if ((tm = (double **) malloc (nr * sizeof (double *))) == NULL)     //Hibakezelés: kifutna a helybőla sorok létrehozásánál
    {
        return -1;
    }

    for (int i = 0; i < nr; ++i)
    {
        if ((tm[i] = (double *) malloc ((i + 1) * sizeof (double))) == NULL)    //Hibakezelés: kifutna a helybőé az elemek létrehozásánál
        {
            return -1;
        }

    }

    for (int i = 0; i < nr; ++i)            //itt feltöltöm a mátrixomat
        for (int j = 0; j < i + 1; ++j)
            tm[i][j] = i * (i + 1) / 2 + j;

    for (int i = 0; i < nr; ++i)            //kiiratom a mátrixomat
    {
        for (int j = 0; j < i + 1; ++j)
            printf ("%f, ", tm[i][j]);
        printf ("\n");
    }

    tm[3][0] = 42.0;        //4.sor 1.eleme
    (*(tm + 3))[1] = 43.0;  //4.sor 2.eleme
    // mi van, ha itt hiányzik a külső ()
    *(tm[3] + 2) = 44.0;    //4.sor 3.eleme
    *(*(tm + 3) + 3) = 45.0;//4.sor 4.eleme

    for (int i = 0; i < nr; ++i)
    {
        for (int j = 0; j < i + 1; ++j)     //kiiratás megint
            printf ("%f, ", tm[i][j]);
        printf ("\n");
    }

    for (int i = 0; i < nr; ++i) //majd felszabadítom a mátrix sorait
        free (tm[i]);

    free (tm);      //majd magát a mátrixot

    return 0;
}